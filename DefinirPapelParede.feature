#language:pt
@DefinirPapelParede
Funcionalidade: papel de parede whatsapp
				Como usu�rio definir papel de parede para tela de conversa��o do WhatsApp, atrav�s do 
				aplicativo instalado em4 um aparelho celular Android
 
Cenario: Listar op��es de papel de parede atrav�s do menu existente na tela de conversa��o
	Dado que eu escolha o papel de parede pode � alterado para uma cor solida pre-definida na tela de conversacao de um contato qualquer
	Quando eu abro a tela de uma conversa
			E clico no menu 'icone de tres pontinhos verticais'
			E clico na op��o Papel de Parede
	Ent�o eu vejo as op��es 'Galeria' 
			 'Cor Solida'
			 'Galeria de Papeis de Parede'
			 'Padrao'
			 'Sem papel de parede'
			
Cenario: Definir papel de parede - Cor S�lida
  Dado que deve ser poss�vel definir papel de parede de cor s�lida
  Quando acionado a op��o "Cor S�lida"
  Ent�o sistema exibe 27 cores como op��o em formato de tabela, de modo a ter 3 cores por linha
   	
  Dado que � poss�vel pr�-visualizar a cor escolhida
  Quando selecionado a cor
 	Ent�o sistema exibe Modo de visualiza��o, contendo uma mensagem recebida/enviada com o texto
 	"Deslize para a esquerda para pr�-visualizar mais cores"
 			E op��es "Cancelar", "Definir"
 	
 	Dado que � poss�vel desistir do modo visualiza��o
 	Quando acionado "Cancelar"
 	Ent�o sistema retorna a lista de cores s�lida
 	
 	Dado que � poss�vel confirmar defini��o da cor para papel de parede 
 	Quando acionado a op��o "Definir"
 	Ent�o sistema atribui a cor escolhida ao papel de fundo de todas as conversas no WhatsApp
		
Cenario: Definir papel de parede - Galeria de fotos

	Dado que � poss�vel selecionar uma foto na galeria de fotos do celular como papel de parede
	Quando acionado a op��o "Galeria"
	Ent�o sistema direciona para galeria de fotos
								 
	Dado que � poss�vel pr�-visualizar a foto escolhida
	Quando selecionada a foto 
	Ent�o sistema exibe uma tela com fundo da foto escolhida 
	E uma mensagem recebida com o texto "Pin�ar para zoom"
	E uma mensagem enviada com o texto "Arraste para ajustar"
	
	Dado que � poss�vel ajustar imagem arrastando no modo de visualiza��o 
	Quando arrastar a imagem pela tela
	Ent�o sistema ajusta a imagem na tela
	
	Dado que � poss�vel ajustar para efetuar zoom na imagem 
	Quando pin�ado na tela 
	Ent�o sistema ajusta a imagem na tela reduzindo/aumentando zoom
	
	Dado que � poss�vel confirmar defini��o da foto para papel de parede 
 	Quando acionado a op��o "Definir"
 	Ent�o sistema atribui a cor escolhida ao papel de fundo de todas as conversas no WhatsApp
   	
Cenario: Definir papel de parede - Galeria WhatsApp

	Dado que � poss�vel selecionar galeria de fotos disponibilizar pelo WhatsApp
	Quando acionado a op��o "Galeria de Pap�is de Parede"
	Ent�o sistema exibe mensagem "Deseja baixar o pacote de papel de parede do WhatsApp?" [Cancelar / OK] 
								 
  Dado que � confirmado baixar o pacote
	Quando acionado a op��o "OK"
	Ent�o sistema direciona para Play Store, para baixar o aplicativo WhatsApp WallPaper
	
	Dado que � poss�vel desistir de baixar o arquivo
	Quando acionado a op��o "Cancelar"
	Ent�o sistema retorna para tela de conversa��o 
			E n�o efetua opera��o
	
Cenario: Definir papel de parede - Padr�o

	Dado que � poss�vel retornar para papel de parede padr�o
	Quando acionado a op��o Papel de Parede > Padr�o
	Ent�o sistema exibe atribu� o papel de parede padr�o do WhatsApp a todas as telas de conversa
	