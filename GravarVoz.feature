# language: pt
@DefinirPapelParede
Funcionalidade: Grava��o de voz e envio para contato ou grupo no WhatsApp (Android)

Cenario: Selecionar contato ou grupo e enviar grava��o de voz

Dado que ao gravar voz 
Quando acionado op��o de �cone de "Microfone"
Ent�o sistema inicia contagem crescente do tempo HH:MM
		E exibe op��o de cancelar grava��o "Deslize para cancelar"

Dado que ao cancelar uma grava��o
Quando acionado op��o de �cone de "Microfone"
		E deslizado para esquerda
Ent�o sistema exibe cancelar grava��o
		E n�o efetua o envio

Dado que ao gravar voz e enviar ao contato desejado
Quando acionado op��o de �cone de "Microfone"
		E soltar o �cone de grava��o
Ent�o sistema envia a mensagem de �udio para o contato desejado